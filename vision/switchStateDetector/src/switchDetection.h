#ifndef SWITCH_DETECTOR_LIB_H
#define SWITCH_DETECTOR_LIB_H

//Standard ROS headers
#include <sensor_msgs/image_encodings.h>
#include <tf/transform_broadcaster.h>

#include <iostream>
#include <string>
#include <fstream>
#include <visualization_msgs/Marker.h>

//OpenCV headers
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include "switch_state_detector/GetState.h"
#include "viewpoint_estimation/ChessboardCorners.h"

#define PDF_TEST__

struct roiWidtHight
{
int x, y, width, hight, offset;
};

class SwitchState_Detector
{
public:
    bool wall;
    cv::Mat I;

    explicit SwitchState_Detector(ros::NodeHandle *nh);
    ~SwitchState_Detector();
   // bool load_calibration_file(std::string filename);
    void image_callback(const sensor_msgs::ImageConstPtr &original_image);
    // subscriber to chessboard corner topic;
    void cbcorner_callback(const viewpoint_estimation::ChessboardCorners &msg);

    bool find_redBlob(switch_state_detector::GetState::Request& distance, switch_state_detector::GetState::Response& response);

    bool find_redBlob_enhenced(switch_state_detector::GetState::Request& distance, switch_state_detector::GetState::Response& response);
//    bool markers_find_pattern(cv::Mat input_image, cv::Mat output_image);
  
//    void publish_marker(geometry_msgs::Pose marker_pose,const int marker_id);

                                      //OpeCV image

private:

#ifdef PDF_TEST
    int serviceCallCoutner;
#endif

    image_transport::Subscriber sub;
    ros::Subscriber sub_CB;

    //Calibration parameters
    std::string filename;                            //calibration file path
    cv::Mat *intrinsics;                             //camera intrinsics
    cv::Mat *distortion_coeff;                       //camera distortion coeffs
    cv::Size *image_size;                            //image_size
    
    cv::Point2f upLeft, upRigh, downLeft, downRight; // corner point of the chessboard

    double marker_size;

};

#endif //SWITCH_DETECTOR_LIB_H
