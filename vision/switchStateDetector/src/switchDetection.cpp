#ifndef SWITCH_DETECTOR_LIB_CPP
#define SWITCH_DETECTOR_LIB_CPP

#include "switchDetection.h"



namespace enc = sensor_msgs::image_encodings;
double degToRad(double degree)
{
	return M_PI*degree/180.0;
}

roiWidtHight getROIsize(cv::Point2f ul, cv::Point2f ur, cv::Point2f dl, cv::Point2f dr, int maxW, int maxH, double angle)
{
	roiWidtHight output;

	int offset = int((ur.x-ul.x)/4.0); 		// 1 cm offset based on CB size

	int width, hight;
	std::cout << "angle_trh= " << degToRad(30.0) << " " << fabs(angle) << std::endl;

	if (fabs(angle) < degToRad(30.0))
	{
	output.x = int(dl.x -offset);
	output.y = int(dl.y);

	width = offset*8;

	hight = offset*17;
	}
	else
	{

		std::cout << " lateral\n";
		output.x = int(dl.x -offset*4);
		output.y = int(dl.y);

		width = offset*16;
		hight = offset*17;

		if (output.x < 0)
			output.x = 0;

	}

	std::cout << "y " << output.y << " x " << output.x << " w " << width << " h " << hight << " o " << offset << "\n";

	if (dl.x - offset + width >= maxW)
	{
		output.width = int(width - ((dl.x - offset + width)-maxW));
//		std::cout << ".";
	}
	else
		output.width = width;

	if (dl.y + hight >= maxH)
	{
		output.hight = int(hight - ((dl.y + hight)- maxH));
//		std::cout << ",";
	}
	else
		output.hight = hight;

	output.offset = offset;

	return output;
}

SwitchState_Detector::SwitchState_Detector(ros::NodeHandle *nh) :
		marker_size(0.1),        //Default Marker size in cm
		filename("empty")       //Initial filename
{
	nh->getParam("/viewpoint_estimation/calibration_file", filename);
	nh->getParam("/viewpoint_estimation/marker_size", marker_size);
	image_transport::ImageTransport it(*nh);
	sub = it.subscribe("/image_raw", 1, &SwitchState_Detector::image_callback,
			this);
	sub_CB = nh->subscribe("chessboardCorners", 1, &SwitchState_Detector::cbcorner_callback, this);

	wall = true;

#ifdef PDF_TEST
    serviceCallCoutner = 0;
#endif
}

SwitchState_Detector::~SwitchState_Detector() {
	delete intrinsics;
	delete distortion_coeff;
	delete image_size;
}

void SwitchState_Detector::cbcorner_callback(const viewpoint_estimation::ChessboardCorners &msg)
{
	upLeft.x  = msg.upLeftX;
	upLeft.y  = msg.upLeftY;

	upRigh.x = msg.upRightX;
	upRigh.y = msg.upRightY;

	downLeft.x = msg.downLeftX;
	downLeft.y = msg.downLeftY;

	downRight.x = msg.downRightX;
	downRight.y = msg.downRightY;
}

void SwitchState_Detector::image_callback(
		const sensor_msgs::ImageConstPtr &original_image) {
	//ROS Image to Mat structure
	cv_bridge::CvImagePtr cv_ptr;
	try {
		cv_ptr = cv_bridge::toCvCopy(original_image, enc::BGR8);
	} catch (cv_bridge::Exception& e) {
		ROS_ERROR("red_ball_detection::cv_bridge_exception %s", e.what());
		this->wall = false;
		return;
	}
	I = cv_ptr->image;
	this->wall = true;

	//imshow("RGB", I);
	//cv::waitKey(10);

}
// detects the biggest red blob on the image
bool SwitchState_Detector::find_redBlob(
		switch_state_detector::GetState::Request& distance,
		switch_state_detector::GetState::Response& response) {
	double ON_probability = 0.01;
	wall = false;
	std::cout << "SW detection service called. \n";
#ifdef PDF_TEST
    std::vector<int> compression_params;
    std::stringstream filename;
    serviceCallCoutner++;
    filename << "image" << this->serviceCallCoutner << ".png";
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);
#endif
	//while (!wall){}
	//std::cout << "over wall. \n";

	cv::Mat img_hsv, img_thresh, im_with_keypoints;
	cv::cvtColor(I, img_hsv, cv::COLOR_BGR2HSV);

	cv::Mat lower_red_hue_range;
	cv::Mat upper_red_hue_range;
	cv::inRange(img_hsv, cv::Scalar(0, 128, 60), cv::Scalar(5, 255, 255),
			lower_red_hue_range);
//	cv::imshow("thresholded Low", lower_red_hue_range);

	cv::inRange(img_hsv, cv::Scalar(190, 100, 100), cv::Scalar(255, 255, 255),
			upper_red_hue_range);

//	cv::imshow("thresholded high", upper_red_hue_range);
	cv::addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0,
			img_thresh);
//	cv::imshow("thresholded Added", img_thresh);

	cv::GaussianBlur(img_thresh, img_thresh, cv::Size(9, 9), 2, 2);

	img_thresh = 255 - img_thresh;
//	cv::imshow("thresholded", img_thresh);
//	cv::waitKey(10);
//	std::cout << "img prepocessed \n";

	cv::SimpleBlobDetector::Params params;
	params.filterByCircularity = true;
	params.minCircularity = 0.7;

	params.filterByConvexity = true;
	params.minConvexity = 0.8;

	params.filterByInertia = true;
	params.minInertiaRatio = 0.01;

	cv::SimpleBlobDetector detector;
	std::vector<cv::KeyPoint> keypoints;
	detector.detect(img_thresh, keypoints);
//	cv::drawKeypoints( I, keypoints, im_with_keypoints, cv::Scalar(0,0,255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
//	std::cout << "detection done \n";

	if (!keypoints.empty()) {
		double max_size = 0.0;
		int id = 0;
		std::vector<cv::KeyPoint> max_key;
		std::vector<cv::KeyPoint>::iterator max_it;

		// we suppose that there is no other red object in the surrounding, so we search for the max size blob.
		for (std::vector<cv::KeyPoint>::iterator it = keypoints.begin();
				it != keypoints.end(); ++it) {
			if (it->size > max_size) {
				max_size = it->size;
				id = it->class_id;
				max_it = it;
			}
		}

		max_key.push_back(*max_it);
		cv::drawKeypoints(I, max_key, im_with_keypoints, cv::Scalar(0, 255, 0),
				cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

		response.stateON = max_size;
		if (1 < max_size) // limiting when it is considered to be ON/OFF, this should depend on distance from switch/chess-board
			response.ON = true;
		else
			response.ON = false;

		cv::imshow("Detection", im_with_keypoints);
		cv::waitKey(10);

#ifdef PDF_TEST
		cv::imwrite(filename.str().c_str(),im_with_keypoints,compression_params);
#endif
	}
	else {
		response.ON = false;
		response.stateON = 0.0;

#ifdef PDF_TEST
		cv::imwrite(filename.str().c_str(),I,compression_params);
#endif
	}
	ROS_INFO("SW state ON %B, blob size %f", response.ON, response.stateON);
	std::cout << "  Switch state detection service terminated. \n\n";
	return true;
}

// focusing only on the right region on the image if chessboard was detected and corner points got published
bool SwitchState_Detector::find_redBlob_enhenced(
		switch_state_detector::GetState::Request& distance,
		switch_state_detector::GetState::Response& response) {
	double ON_probability = 0.01;
	wall = false;
	std::cout << "SW detection service called. \n";

#ifdef PDF_TEST
    std::vector<int> compression_params;
    std::stringstream filename;
    serviceCallCoutner++;
    filename << "image" << this->serviceCallCoutner << ".png";
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);
#endif
	//while (!wall){}
	//std::cout << "over wall. \n";

	cv::Mat img_hsv, img_thresh, im_with_keypoints;

	//std::cout << int(upLeft.x) << " " << int(upLeft.y) << " " << int(downRight.x -downLeft.x) << " " << int(downLeft.y - upLeft.y) << " " << I.cols << std::endl;

	roiWidtHight roiSetup = getROIsize(upLeft, upRigh, downLeft, downRight, I.cols, I.rows, distance.distance);

//	std::cout << roiSetup.x << " " <<  roiSetup.y << " " << roiSetup.width << " " << roiSetup.hight << std::endl;

	cv::Mat img_roi = I(cv::Rect(roiSetup.x, roiSetup.y, roiSetup.width, roiSetup.hight));

	cv::imshow("ROI", img_roi);
	cv::waitKey(10);

	//std::cout << "ROI\n";
	cv::cvtColor(img_roi, img_hsv, cv::COLOR_BGR2HSV);

	cv::Mat lower_red_hue_range;
	cv::Mat upper_red_hue_range;
	cv::inRange(img_hsv, cv::Scalar(0, 128, 60), cv::Scalar(5, 255, 255),
			lower_red_hue_range);
//	cv::imshow("thresholded Low", lower_red_hue_range);

	cv::inRange(img_hsv, cv::Scalar(190, 100, 100), cv::Scalar(255, 255, 255),
			upper_red_hue_range);

//	cv::imshow("thresholded high", upper_red_hue_range);
	cv::addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0,
			img_thresh);
//	cv::imshow("thresholded Added", img_thresh);

	cv::GaussianBlur(img_thresh, img_thresh, cv::Size(9, 9), 2, 2);

	img_thresh = 255 - img_thresh;
//	cv::imshow("thresholded", img_thresh);
//	cv::waitKey(10);
//	std::cout << "img prepocessed \n";

	cv::SimpleBlobDetector::Params params;
	params.filterByCircularity = true;
	params.minCircularity = 0.7;

	params.filterByConvexity = true;
	params.minConvexity = 0.8;

	params.filterByInertia = true;
	params.minInertiaRatio = 0.01;

	cv::SimpleBlobDetector detector;
	std::vector<cv::KeyPoint> keypoints;
	detector.detect(img_thresh, keypoints);
//	cv::drawKeypoints( I, keypoints, im_with_keypoints, cv::Scalar(0,0,255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
//	std::cout << "detection done \n";

	if (!keypoints.empty()) {
		double max_size = 0.0;
		int id = 0;
		std::vector<cv::KeyPoint> max_key;
		std::vector<cv::KeyPoint>::iterator max_it;

		// we suppose that there is no other red object in the surrounding, so we search for the max size blob.
		for (std::vector<cv::KeyPoint>::iterator it = keypoints.begin();
				it != keypoints.end(); ++it) {
			if (it->size > max_size) {
				max_size = it->size;
				id = it->class_id;
				max_it = it;
			}
		}

		max_key.push_back(*max_it);
		cv::drawKeypoints(img_roi, max_key, im_with_keypoints, cv::Scalar(0, 255, 0),
				cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

		response.stateON = max_size;
		if (1 < max_size) // limiting when it is considered to be ON/OFF, this should depend on distance from switch/chess-board
			response.ON = true;
		else
			response.ON = false;

		cv::imshow("Detection", im_with_keypoints);
		cv::waitKey(10);

#ifdef PDF_TEST
		cv::imwrite(filename.str().c_str(),im_with_keypoints,compression_params);
#endif
	}
	else {
		response.ON = false;
		response.stateON = 0.0;

#ifdef PDF_TEST
		cv::imwrite(filename.str().c_str(),img_roi,compression_params);
#endif
	}
	ROS_INFO("SW state ON %B, blob size %f", response.ON, response.stateON);
	std::cout << "  Switch state detection service terminated. \n\n";
	return true;
}
#endif
