#include <ros/ros.h>
#include "switchDetection.h"

void add()
{
}

int 
main(int argc, char *argv[])
{
  ros::init(argc, argv, "switch_state_detection");
  ros::NodeHandle nh;
  ros::AsyncSpinner spinner(0);
  spinner.start();

  SwitchState_Detector detector(&nh);

  ros::ServiceServer service = nh.advertiseService("detect_switch_state",
			&SwitchState_Detector::find_redBlob, &detector);

  ros::ServiceServer service2 = nh.advertiseService("detect_switch_state_enhenced",
  			&SwitchState_Detector::find_redBlob_enhenced, &detector);


  std::cout << "Observer Services ON \n \n ";
  ros::waitForShutdown();
}
