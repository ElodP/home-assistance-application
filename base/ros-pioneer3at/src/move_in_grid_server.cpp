#include <ros/ros.h>
#include "pioneer3at/MoveInGrid.h"
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>



typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

move_base_msgs::MoveBaseGoal goal;
bool new_request;

bool move(pioneer3at::MoveInGrid::Request  &req,
		pioneer3at::MoveInGrid::Response &res)
{
	ROS_INFO("request: x=%f, y=%f", (float)req.x, (float)req.y);
	move_base_msgs::MoveBaseGoal goal;

	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();

	// TODO: change these lines to correct the movement ESNW
	goal.target_pose.pose.position.x = req.x;
	goal.target_pose.pose.position.y = req.y;

	goal.target_pose.pose.orientation.z = 0;
	goal.target_pose.pose.orientation.w = 1;
	
	// interblocking not implemented !!!
	new_request = true;
	return true;
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "move_in_grid_server");
	ros::NodeHandle n;

	ros::ServiceServer service = n.advertiseService("move_in_grid", move);
	ROS_INFO("Ready to move robot in the grid!");
	std::string move_base="move_base";


	//tell the action client that we want to spin a thread by default
	MoveBaseClient ac(move_base, true);

	//wait for the action server to come up
	while(!ac.waitForServer(ros::Duration(5.0))){
		ROS_INFO("Waiting for the move_base action server to come up");
	}

	new_request = false;

	while(ros::ok())
	{
		ros::spinOnce();
		if (new_request)
		{ 
			ROS_INFO("Sending goal");
			ac.sendGoal(goal);

			ac.waitForResult();

			if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
			{
				ROS_INFO("Got to the goal");
			}
			else
			{
				ROS_INFO("Failed to got to the goal");
			}
			new_request = false;
		}
		sleep(1);
	}

	return 0;
}
