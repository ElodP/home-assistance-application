#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>


std::string move_base;
double x,y,z,w;



typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int main(int argc, char** argv){
  ros::init(argc, argv, "simple_navigation_goals");

  ros::NodeHandle n_("navigoal");

  n_.param<std::string>("/Pioneer3AT_navigoal/move_base", move_base,   "/Pioneer3AT/move_base");
  n_.param<double>("/Pioneer3AT_navigoal/x", x,  0.0);
  n_.param<double>("/Pioneer3AT_navigoal/y", y,  0.0);
  n_.param<double>("/Pioneer3AT_navigoal/z", z,  0.00);
  n_.param<double>("/Pioneer3AT_navigoal/w", w,  1.00);




  //tell the action client that we want to spin a thread by default
  MoveBaseClient ac(move_base, true);

  //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  move_base_msgs::MoveBaseGoal goal;

  //we'll send a goal to the robot to move 1 meter forward
  goal.target_pose.header.frame_id = "Pioneer3AT/map";
  goal.target_pose.header.stamp = ros::Time::now();

  goal.target_pose.pose.position.x = x;
  goal.target_pose.pose.position.y = y;

  goal.target_pose.pose.orientation.z = z;
  goal.target_pose.pose.orientation.w = w;

  sleep(20);

  ROS_INFO("Sending goal");
  ac.sendGoal(goal);

  ac.waitForResult();

  if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    ROS_INFO("Got to the box");
  else
    ROS_INFO("The base failed to move for some reason");

  return 0;
}
