#include "ros/ros.h"
#include "pioneer3at/MoveInGrid.h"
#include <cstdlib>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "move_in_grid_client");
  if (argc != 3)
  {
    ROS_INFO("usage: move dir dist");
    return 1;
  }

  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<pioneer3at::MoveInGrid>("move_in_grid");
  pioneer3at::MoveInGrid srv;
  srv.request.x = atof(argv[1]);
  srv.request.y = atof(argv[2]);
  if (client.call(srv))
  {
    ROS_INFO("Success: %d\n", (bool)srv.response.success?0:1);
  }
  else
  {
    ROS_ERROR("Failed to call service add_two_ints");
    return 1;
  }

  return 0;
}
