/*
 * ArmControl.cpp
 *
 *  Created on: Sep 21, 2015
 *      Author: elod
 */

#include "ArmControl.h"

template<typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

double radToDeg(double radian) {
	return radian * 180.0 / M_PI;
}
double degToRad(double degree) {
	return degree / 180.0 * M_PI;
}
tf::Quaternion roatetQauternion(tf::Quaternion orientation,
		tf::Quaternion rotation) {
	tf::Quaternion res;
	res.getIdentity();
	res *= rotation;
	res *= orientation;
	res *= rotation.inverse();

	return rotation * orientation * rotation.inverse();
}
void printPosition(geometry_msgs::Pose p, std::string lable) {
	tf::Pose p_;
	double roll, pitch, yaw, ox, oy, oz;
	tf::poseMsgToTF(p, p_);

	ox = p_.getOrigin().x();
	oy = p_.getOrigin().y();
	oz = p_.getOrigin().z();

	p_.getBasis().getEulerYPR(yaw, pitch, roll);

	std::cout << lable << ":\n";
	std::printf("pos[x,y,z]=[%f, %f, %f] ori[x,y,z,w]=[%f, %f, %f, %f] \n",
			ox, oy, oz, p.orientation.x, p.orientation.y, p.orientation.z,
			p.orientation.w);
}

double distance(geometry_msgs::Point from,  tf::Vector3 to)
{
	double disance = sqrt((from.x - to.x())*(from.x - to.x()) + (from.y - to.y())*(from.y - to.y()) );
	return disance;
}
ArmControl::ArmControl() :
		nh_("~"), planning_group_name_("arm"), POS_TOLARENCE(0.005), ANG_TOLARENCE(
				0.08), PLANING_TIME(45.0), base_reference("base_link") {

	move_group_.reset(
			new move_group_interface::MoveGroup(planning_group_name_));
	move_group_->setPlanningTime(PLANING_TIME);
	move_group_->setGoalOrientationTolerance(ANG_TOLARENCE);
	move_group_->setGoalPositionTolerance(POS_TOLARENCE);
	move_group_->setPlannerId("RRTConnectkConfigDefault");

	this->collisionBoxesPresent = false;
	this->eef = move_group_->getEndEffectorLink();
	moveToInitialPos();
	chessBoardFound = false;
	sleep(5);
	j0_zeroPoint = getJointValue(0);
//	cout << "initial j0 = " << radToDeg(j0_zeroPoint) << endl;
}

ArmControl::~ArmControl() {
	collisionCB_id.erase(collisionCB_id.begin(),collisionCB_id.end());

}

bool ArmControl::greaterPositonDifference(tf::StampedTransform cbPose, double diffX, double diffY, double diffZ)
{

	if (abs(cbPose.getOrigin().x() - move_group_->getCurrentPose().pose.position.x) > diffX)
		return true;

	if (abs(cbPose.getOrigin().y() - move_group_->getCurrentPose().pose.position.y) > diffY)
		return true;

	if (abs(cbPose.getOrigin().z() - move_group_->getCurrentPose().pose.position.z) > diffZ)
		return true;

	return false;
}

double ArmControl::getJointValue(int index)
{
	std::vector<double> joint_values;
	move_group_->getCurrentState()->copyJointGroupPositions(
			move_group_->getCurrentState()->getRobotModel()->getJointModelGroup(
					move_group_->getName()), joint_values);

	return joint_values[index];
}

bool ArmControl::moveToPos(geometry_msgs::Pose target) {
	move_group_->setPoseTarget(target);
	move_group_->setStartState(*move_group_->getCurrentState());

	bool success = move_group_->plan(move_plan);
	ROS_INFO("Visualizing plan 1 (pose goal) %s", success ? "" : "FAILED");
//	sleep(5.0);
	if (success) {
		move_group_->move(); //execute(move_plan);
		sleep(5.0);
	} else {
		ROS_ERROR(
				"Error while planning position of arm, no move command was sent!\n");
		printPosition(target, "coudn't reach:");
		return false;
	}
	return true;

}

geometry_msgs::Pose ArmControl::moveToInitialPos() {
	geometry_msgs::Pose target_init;
	// without the mobile base the initial position is:
	// wxyz  0.7071 -0.7071 0.0 0.0; xyz = 0.1 0.1 0.3;
	target_init.orientation.w = -0.49089;
	target_init.orientation.x = 0.52124;
	target_init.orientation.y = -0.5074;
	target_init.orientation.z = 0.47945;
	target_init.position.x = 0.13;
	target_init.position.y = 0.0;
	target_init.position.z = 0.93;
	moveToPos(target_init);
	return target_init;
}

geometry_msgs::Pose ArmControl::rotateEefTo(double radian){
	std::vector<double> joint_values;

	// Orientation constrain adding
	moveit_msgs::Constraints flip_constrains;
//	moveit_msgs::OrientationConstraint flip_orientation;
//	flip_orientation.link_name = eef;
//	flip_orientation.header.frame_id = base_reference;
//	flip_orientation.orientation =
//			move_group_->getCurrentPose().pose.orientation;
//	flip_orientation.orientation.z = 0;
//
//	flip_orientation.absolute_x_axis_tolerance = 0.05;
//	flip_orientation.absolute_y_axis_tolerance = 0.05;
//	flip_orientation.absolute_z_axis_tolerance = 0.05;
//	flip_orientation.weight = 0.7;
//	flip_constrains.orientation_constraints.push_back(flip_orientation);
//	// Position constrain adding
//	moveit_msgs::PositionConstraint flip_position;
//	flip_position.link_name = eef;
//	flip_position.header.frame_id = base_reference;
//	flip_position.target_point_offset.x = move_group_->getCurrentPose().pose.position.x;
//	flip_position.target_point_offset.y = move_group_->getCurrentPose().pose.position.y;
//	flip_position.target_point_offset.z = move_group_->getCurrentPose().pose.position.z;
//	flip_position.weight = 1.0;
//
//	flip_constrains.position_constraints.push_back(flip_position);
//	move_group_->setPathConstraints(flip_constrains);

	geometry_msgs::Pose target;



	target.position = move_group_->getCurrentPose().pose.position;
	vector<double> orientation =  move_group_->getCurrentRPY();
	tf::Quaternion q;
	q.setRPY(orientation[0], orientation[1], orientation[2] + radian );

	tf::quaternionTFToMsg(q, target.orientation);
	moveToPos(target);

    sleep(5.0);
    move_group_->clearPathConstraints();
	return target;
}

geometry_msgs::Pose ArmControl::rotateBaseTo(double radian){
	std::vector<double> joint_values;
	geometry_msgs::Pose current_p;
	move_group_->getCurrentState()->copyJointGroupPositions(
			move_group_->getCurrentState()->getRobotModel()->getJointModelGroup(
					move_group_->getName()), joint_values);

	joint_values[0] = j0_zeroPoint + radian;

	move_group_->setStartState(*move_group_->getCurrentState());
	move_group_->setJointValueTarget(joint_values);
	bool success = move_group_->plan(move_plan);
	move_group_->execute(move_plan);
	current_p = move_group_->getCurrentPose().pose;
    sleep(5.0);

	return current_p;
}

geometry_msgs::Pose ArmControl::rotateBase(double degree) {

	std::vector<double> joint_values;
//	geometry_msgs::Pose current_p;

	move_group_->getCurrentState()->copyJointGroupPositions(
			move_group_->getCurrentState()->getRobotModel()->getJointModelGroup(
					move_group_->getName()), joint_values);
	joint_values[0] = joint_values[0] - degToRad(degree);

	move_group_->setStartState(*move_group_->getCurrentState());
	move_group_->setJointValueTarget(joint_values);
	bool success = move_group_->plan(move_plan);

	if (!success)
		ROS_ERROR("Base rotation failed!");
	else
		move_group_->execute(move_plan);


	sleep(5.0);
	return move_group_->getCurrentPose().pose;
}

// we only change the orientation angel of the eef and not the position.
geometry_msgs::Pose ArmControl::orientateArmToCB(geometry_msgs::Pose CB_pose,
		double yawError) {

	geometry_msgs::Pose target_init;

	target_init.orientation = CB_pose.orientation;
	target_init.position = move_group_->getCurrentPose().pose.position;

	if (moveToPos(target_init))
	{
		cout << "-orientation terminated! \n";
		return target_init;
	}
	else
	{
		ROS_ERROR("Orientation of arm to chess board failed! \n");
	}

//	target_init.orientation = this->move_group_->getCurrentPose().pose.orientation;
//	target_init.position = this->move_group_->getCurrentPose().pose.position;

	return target_init;
}


ArmControl::posioint_correction ArmControl::getTargetPoseFromChessBoard(
		geometry_msgs::Pose &current_pose, tf::StampedTransform chessB_pose) {

	// set the absolute coordinates of the CB in Pose
	double angle_threshol = degToRad(10.0);
	tf::Pose p_;
	p_.setOrigin(chessB_pose.getOrigin());
	p_.setRotation(p_.getRotation().normalize());
	tf::poseTFToMsg(p_, current_pose);

	// absolute rotation such that the arm parallel with CB
	double ro, po, yo;
	double r, p, y, refR = M_PI, refP = 0.0, refY = M_PI / 2.0, errR, errY,
			errP;
	tf::Quaternion q_orientation, q_rotation;
	tf::poseMsgToTF(move_group_->getCurrentPose().pose, p_);
	p_.getBasis().getRPY(ro, po, yo);
	chessB_pose.getBasis().getRPY(r, p, y);

	errR = sgn(r) * refR - r;
	errP = sgn(p) * refP - p;
	errY = sgn(y) * refY - y;

	ArmControl::posioint_correction correction;
	correction.x = 0;
	correction.y = 0;
	correction.z = 0;

	if (abs(errR) < angle_threshol) {
		errR = 0.0;
	}else{
//		printf("  Roll error %f, = > %f \n", radToDeg(errR), correction.z);
//		correction.z = errP * 0.35;
	}

	if (abs(errP) < angle_threshol) {
		errP = 0.0;
	}else
	{
		double distance = chessB_pose.getOrigin().z()*chessB_pose.getOrigin().z() + chessB_pose.getOrigin().y()*chessB_pose.getOrigin().y();
		double realDistance= sqrt(distance) * sin(errP);
		correction.y = errP * 0.2;

		printf("distance = %f, z = %f, y = %f, x = %f \n", sqrt(distance), chessB_pose.getOrigin().z(), chessB_pose.getOrigin().y(), chessB_pose.getOrigin().x() );

		printf("  Pitch error %f, = > %f or %f \n", radToDeg(errP), correction.y, realDistance);

	}

	if (abs(errY) < angle_threshol) {
		errY = 0.0;
	}

	ro = ro + errY;
	po = po - errR;
	yo = yo + errP;

	printf("Angle error r,p,y: %f, %f, %f\n", radToDeg(errR), radToDeg(errP), radToDeg(errY));
	q_orientation.setRPY(ro, po, yo);
	tf::quaternionTFToMsg(q_orientation, current_pose.orientation);

	return correction;
}

std::vector<moveit_msgs::CollisionObject> ArmControl::moveCollisionObjChessboard() {
	std::vector<moveit_msgs::CollisionObject> collision_objects;
	moveit_msgs::CollisionObject chessBoard_obj, switch_obj, panel_obj;
	chessBoard_obj.id = "ChessBoard";
	switch_obj.id = "Switch";
	panel_obj.id = "Panel";
	chessBoard_obj.operation = chessBoard_obj.MOVE;
	panel_obj.operation = panel_obj.MOVE;
	switch_obj.operation = switch_obj.MOVE;

	collision_objects.push_back(chessBoard_obj);
	collision_objects.push_back(panel_obj);
	collision_objects.push_back(switch_obj);

	return collision_objects;
}

std::vector<moveit_msgs::CollisionObject> ArmControl::creatCollisionObjChessboard(
		tf::StampedTransform cameraGrid, bool cheat) {

	// "cheat" means that we push the collision box further from the base
	//in order to eliminate the CB detection error, so we can push the button
	//This is under testing. If the error is cst we can use this patch!

	if (!collisionCB_id.empty())
		collisionCB_id.erase(collisionCB_id.begin());

	this->collisionBoxesPresent = true;
	double cheatVal = 0.015;
	tf::Quaternion q_;
	moveit_msgs::CollisionObject chessBoard_obj, switch_obj, panel_obj;
	std::vector<moveit_msgs::CollisionObject> collision_objects;
	chessBoard_obj.header.frame_id = "calibration_grid"; //group.getPlanningFrame();
	switch_obj.header.frame_id = "calibration_grid";
	panel_obj.header.frame_id = "calibration_grid";

	// The id of the object is used to identify it.
	chessBoard_obj.id = "ChessBoard";
	switch_obj.id = "Switch";
	panel_obj.id = "Panel";

	// Define a box to add to the world.
	shape_msgs::SolidPrimitive primitive;
	primitive.type = primitive.BOX;
	// A pose for the box (specified relative to frame_id)
	geometry_msgs::Pose box_pose;
//	cameraGrid.getBasis().getRotation(q_);
//	q_.normalize();

//====== define dim and center of CB
	primitive.dimensions.resize(3);
	primitive.dimensions[0] = 0.06;  // seams to be x
	primitive.dimensions[1] = 0.075; // seams to be y
	primitive.dimensions[2] = 0.005;  // seams to be z
	box_pose.orientation.w = 1; //cameraGrid.getRotation().getW();
	box_pose.orientation.x = 0; //cameraGrid.getRotation().getX();
	box_pose.orientation.y = 0; //cameraGrid.getRotation().getY();
	box_pose.orientation.z = 0; //cameraGrid.getRotation().getZ();
	box_pose.position.x = 0.015;
	box_pose.position.y = 0.0225;
	box_pose.position.z = 0.00;
	if (cheat)
		box_pose.position.z = box_pose.position.z - cheatVal; // 1cm pushed further
	// collision object parameter setup
	chessBoard_obj.primitives.push_back(primitive);
	chessBoard_obj.primitive_poses.push_back(box_pose);
	chessBoard_obj.operation = chessBoard_obj.ADD;
	// add collision object
	// reduce number of collision objects for planning performance increase
//	collision_objects.push_back(chessBoard_obj);
//	collisionCB_id.push_back(chessBoard_obj.id);

//========== define panel dimensions and origin
	primitive.dimensions[0] = 0.24;  // seams to be x
	primitive.dimensions[1] = 0.19; // seams to be y
	primitive.dimensions[2] = 0.145;  // seams to be z
	box_pose.position.x = 0.088;
	box_pose.position.y = 0.023;
	box_pose.position.z = -0.0725;
	if (cheat)
		box_pose.position.z = box_pose.position.z - cheatVal; // 1cm pushed further
	// collision object parameter setup
	panel_obj.primitives.push_back(primitive);
	panel_obj.primitive_poses.push_back(box_pose);
	panel_obj.operation = panel_obj.ADD;
	// add collision object
	collision_objects.push_back(panel_obj);
	collisionCB_id.push_back(panel_obj.id);

//========== define switch dimensions and origin
	primitive.dimensions[0] = 0.08;  // seams to be x
	primitive.dimensions[1] = 0.08; // seams to be y
	primitive.dimensions[2] = 0.009;  // seams to be z
	box_pose.position.x = 0.04 + 0.003 + 3 * 0.015;
	box_pose.position.y = 0.04 + 0.003 - 0.015;
	box_pose.position.z = 0.0045;
	// collision object parameter setup
	switch_obj.primitives.push_back(primitive);
	switch_obj.primitive_poses.push_back(box_pose);
	switch_obj.operation = switch_obj.ADD;
	// add collision object
//	collision_objects.push_back(switch_obj);
//	collisionCB_id.push_back(switch_obj.id);
//
	return collision_objects;
}

bool ArmControl::findCB(arm_action_server::FindChessBoard::Request& req,
		arm_action_server::FindChessBoard::Response& res) {

	cout << "--Search CB around robot \n";
	bool CBfound = false;
	int rotation_counter = 0;
	double degrees = 20;
	// delet Chess Board collision object when new information is read
	tf::StampedTransform CBinArmFrame;

	this->current_pos = this->moveToInitialPos();

//	if (req.yaw != 0.0)
//	cout << "roate to req.yaw: \n";
	rotateEefTo(req.yaw);

	while (!CBfound) {
		cout << ".. ";
		if (this->chessBoard_listener.waitForTransform("/calibration_grid", eef,
				ros::Time(0), ros::Duration(7.0)))
		{
			//CB found
			std::cout << "CB found \n";
			CBfound = true;
			chessBoardFound = true;
			try {

				this->chessBoard_listener.lookupTransform("base_link_cyton", "/calibration_grid",  ros::Time(0), CBinArmFrame);

				 double yaw = atan2(CBinArmFrame.getOrigin().y(), CBinArmFrame.getOrigin().x());

				 printf("cd coord (x,y): %f, %f \n",CBinArmFrame.getOrigin().x(), CBinArmFrame.getOrigin().y());
				 cout << "CB yaw:" << radToDeg(yaw -j0_zeroPoint) << endl;
				 cout << "roate to CB: \n";
				 // this might not work due to particular case pi/2, it should be dependent of j0
				 rotateBaseTo(M_PI/2.0 + yaw);

			} catch (tf::TransformException ex) {
				ROS_ERROR("%s", ex.what());
				cout << "\n ERROR while TF CB in arm frame \n";
				moveToInitialPos();
				return false;
			}

		} else {
			//CB not found, rotate arm
//			std::cout << "CB not found \n";
			if (rotation_counter == 0)
				rotateEefTo(degToRad(-60.0));
//			if (getJointValue(0) - j0_zeroPoint <= degToRad(-90.0))
//				rotateBaseTo(degToRad(90.0));
			rotation_counter++;
			rotateEefTo(degrees); // write function rotate base x degrees!
		}
		if (rotation_counter * degrees > 120) {
			this->planning_scene_interface.removeCollisionObjects(
					this->collisionCB_id);
			this->collisionBoxesPresent = false;
			sleep(2.0);
			res.moveAccomplished = 0.0;
			chessBoardFound = false;
			return false;
		}
	}

//!!!!******** BUGGGG: if it is called more times, the previous CB position is used not the current one
//where the collision boxes are placed ***********!!!!!!!
	// add newly aquired object position
	this->planning_scene_interface.addCollisionObjects(
			creatCollisionObjChessboard(transform_cameraToGrid, false));
	sleep(3.0);
	cout << "--Collision box added. \n";

	res.distance = distance(move_group_->getCurrentPose().pose.position, CBinArmFrame.getOrigin());

	std::vector<double> joint_values;
	move_group_->getCurrentState()->copyJointGroupPositions(
			move_group_->getCurrentState()->getRobotModel()->getJointModelGroup(
					move_group_->getName()), joint_values);
	res.yaw = joint_values[0];
	res.moveAccomplished = 1.0;

	cout << "service exiting \n\n";

	return true;

}

bool ArmControl::flippSW(arm_action_server::FlippSwitch::Request& req,
		arm_action_server::FlippSwitch::Response& res) {

	tf::StampedTransform WRtransfrom_inCBframe, CBinArmFrame;
	geometry_msgs::Pose target;
	double yawError = 0.0;
	ArmControl::posioint_correction correction;

	//wait for TF CB appear on img
	if (this->chessBoard_listener.waitForTransform("/calibration_grid", eef,
			ros::Time(0), ros::Duration(3.0))) {
		//orientate arm to match CB
		double prior_distance = 0.1, contact_distance = 0.01, contact = 0.005;
		double CB_x_diff = 0.01, CB_y_diff = 0.01, CB_z_diff = 0.01;
		try {
			// tf for arm orientation
			this->chessBoard_listener.lookupTransform("/calibration_grid", eef,
					ros::Time(0), WRtransfrom_inCBframe);
			this->chessBoard_listener.lookupTransform(base_reference,
					"/calibration_grid", ros::Time(0), CBinArmFrame);

			correction = this->getTargetPoseFromChessBoard(target_pos,
					WRtransfrom_inCBframe);

			this->current_pos = this->orientateArmToCB(target_pos, yawError);

		} catch (tf::TransformException ex) {
			ROS_ERROR("%s", ex.what());
			cout << "\n ERROR while TF eff in CB frame \n";
			moveToInitialPos();
			return false;
		}
//		sleep(3.0);

//		try {
//
//			this->chessBoard_listener.lookupTransform(base_reference,
//					"/calibration_grid", ros::Time(0), CBinArmFrame);
//		} catch (tf::TransformException ex) {
//			ROS_ERROR("%s", ex.what());
//			cout << "\n ERROR while TF CB in arm frame \n";
//			moveToInitialPos();
//			return false;
//		}

		//go closer to CB so position error is less
		printPosition(this->move_group_->getCurrentPose().pose,  "oriented pos");
		// Target position moving closer to CB 11cm
		target.position.x = CBinArmFrame.getOrigin().getX() - 0.15;
		target.position.y = CBinArmFrame.getOrigin().getY() - 0.01;  // - scorrection.y;
		target.position.z = CBinArmFrame.getOrigin().getZ() - 0.0; 	 //req.yaw; // - correction.z;

		target.orientation = current_pos.orientation; //move_group_->getCurrentPose().pose.orientation;
		cout << "1st CB x = " << CBinArmFrame.getOrigin().getX() << endl;
		if (!moveToPos(target))
		{
			ROS_ERROR("Moving arm closer to CB FAILED!");
			return false;
		}
		printPosition(this->move_group_->getCurrentPose().pose,  "closeup pos");

		sleep(3.0);
		//re-position collision box with less error in pose
		try {

			this->chessBoard_listener.lookupTransform(base_reference,
					"/calibration_grid", ros::Time(0), CBinArmFrame);
			this->planning_scene_interface.addCollisionObjects(
					creatCollisionObjChessboard(CBinArmFrame, true));
			sleep(3.0);
			cout << "2nd CB x = " << CBinArmFrame.getOrigin().getX()
					<< endl;
		} catch (tf::TransformException ex) {
			ROS_ERROR("Close-up feedback - %s", ex.what());
//			moveToInitialPos();
//			return false;
		}

		if (greaterPositonDifference(CBinArmFrame, 0.15, 0.01, req.yaw)) {
			cout<< "- correction of closeup position needed\n";
			// Target position moving closer to CB 11cm
			target.position.x = CBinArmFrame.getOrigin().getX() - 0.15;
			target.position.y = CBinArmFrame.getOrigin().getY() - 0.01; // - scorrection.y;
			target.position.z = CBinArmFrame.getOrigin().getZ() - 0.0;  //req.yaw; // - correction.z;
			target.orientation = current_pos.orientation; //move_group_->getCurrentPose().pose.orientation;
			if (!moveToPos(target)) {
				ROS_ERROR("Moving arm clos correction to CB FAILED!");
				return false;
			}
			printPosition(this->move_group_->getCurrentPose().pose,
					"correction pos");
			sleep(3.0);
		}

		// Orientation constrain adding
		moveit_msgs::Constraints flip_constrains;

		moveit_msgs::OrientationConstraint flip_orientation;
		flip_orientation.link_name = eef;
		flip_orientation.header.frame_id = base_reference;
		flip_orientation.orientation =
				move_group_->getCurrentPose().pose.orientation;
		flip_orientation.absolute_x_axis_tolerance = 0.05;
		flip_orientation.absolute_y_axis_tolerance = 0.05;
		flip_orientation.absolute_z_axis_tolerance = 0.05;
		flip_orientation.weight = 0.7;
		flip_constrains.orientation_constraints.push_back(flip_orientation);
		// Position constrain adding
		moveit_msgs::PositionConstraint flip_position;
		flip_position.link_name = eef;
		flip_position.header.frame_id = base_reference;
		flip_position.target_point_offset.x = 0; //target.position.x;
		flip_position.target_point_offset.y = target.position.y;
		flip_position.target_point_offset.z = target.position.z;
		flip_position.weight = 1.0;

		flip_constrains.position_constraints.push_back(flip_position);
		move_group_->setPathConstraints(flip_constrains);

		cout << "flip action: \n";
		target.position.x = CBinArmFrame.getOrigin().getX() - req.distance; //0.11
		target.orientation = move_group_->getCurrentPose().pose.orientation;

		if (moveToPos(target)) {
			res.moveAccomplished = 1.0;
			res.successful = true;
		} else {
			res.moveAccomplished = 0.90;
			res.successful = false;
			ROS_ERROR("Flip action was not performed!\n");
		}
		move_group_->clearPathConstraints();

	} else {
		ROS_ERROR("Chess board is not in visual range!\n");
		moveToInitialPos();
		return false;
	}
	cout << "service terminated successfully \n\n";
	moveToInitialPos();
	return true;
}
