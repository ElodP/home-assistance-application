#include "ArmControl.h"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "arm_action_server");
	ros::NodeHandle nh;
	ros::AsyncSpinner spinner(0);
	spinner.start();

	ArmControl controller;
	// only for testing, maybe good alaso in the future.
//	controller.moveToInitialPos();

	ros::ServiceServer findCB_service = nh.advertiseService("find_chess_board",
					&ArmControl::findCB, &controller);

	ros::ServiceServer flippSW_service = nh.advertiseService("flipp_switch",
						&ArmControl::flippSW, &controller);

	cout << "Arm Services ON \n \n ";
	ros::waitForShutdown();

}
