/*
 * ArmControl.h
 *
 *  Created on: Sep 21, 2015
 *      Author: elod
 */


#ifndef ARMCONTROL_H_
#define ARMCONTROL_H_
#include "ros/ros.h"

// Service server
#include "arm_action_server/FindChessBoard.h"
#include "arm_action_server/FlippSwitch.h"

// MoveIt!
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group.h>
#include <shape_tools/solid_primitive_dims.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

//TF listener
#include <tf/transform_listener.h>
#include <tf/tf.h>

//Math
#include <math.h>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;


class ArmControl {
public:

	ros::NodeHandle nh_;
	std::string planning_group_name_;
	double POS_TOLARENCE, ANG_TOLARENCE, PLANING_TIME;
	// interface with MoveIt
	boost::scoped_ptr<move_group_interface::MoveGroup> move_group_;

	geometry_msgs::Pose chessBoardPose;
	bool chessBoardFound;
	// the zero position of the base joint depends of startup position of the arm
	double j0_zeroPoint;

	ArmControl();
	virtual ~ArmControl();

	bool findCB(arm_action_server::FindChessBoard::Request& req,
			arm_action_server::FindChessBoard::Response& res);

	bool flippSW(arm_action_server::FlippSwitch::Request& req,
			arm_action_server::FlippSwitch::Response& res);

	geometry_msgs::Pose moveToInitialPos();

private:
	struct posioint_correction{
		double x,y,z;
	};

	string eef, base_reference;
	bool collisionBoxesPresent;
	geometry_msgs::Pose current_pos, target_pos;
	moveit::planning_interface::MoveGroup::Plan move_plan;
	moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
	std::vector<std::string> collisionCB_id;

	tf::TransformListener chessBoard_listener;
	tf::StampedTransform transform_cameraToGrid;

	bool greaterPositonDifference(tf::StampedTransform cbPose, double diffX, double diffY, double diffZ);
	double getJointValue(int index);
	std::vector<moveit_msgs::CollisionObject> moveCollisionObjChessboard();

	bool moveToPos(geometry_msgs::Pose target);
	geometry_msgs::Pose rotateBase(double degree);
	geometry_msgs::Pose rotateBaseTo(double radian);
	geometry_msgs::Pose rotateEefTo(double radian);
	posioint_correction getTargetPoseFromChessBoard(
			geometry_msgs::Pose &current_pose, tf::StampedTransform chessB_pose);
	std::vector<moveit_msgs::CollisionObject> creatCollisionObjChessboard(
			tf::StampedTransform cameraGrid, bool cheat);
	geometry_msgs::Pose orientateArmToCB(geometry_msgs::Pose CB_pose, double yawError);

};

#endif /* ARMCONTROL_H_ */
