#include "ros/ros.h"

#include <stdbool.h>
#include <cstdlib>
#include "std_msgs/String.h"
#include <vector>
#include <math.h>
#include <string>
#include <sstream>

// switch state sensing service mdg
#include "switch_state_detector/GetState.h"
// chessboard serach service msg
#include "arm_action_server/FindChessBoard.h"
// flipp action service msg
#include "arm_action_server/FlippSwitch.h"
// pomdp planner service msg
#include "pomdp_planner/PlannerAction.h"
//transform listener libs
#include <tf/transform_listener.h>
#include <tf/tf.h>

// p3at navigation libs
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

//#define MAX_OBSERVATION_DISTANCE 1.4
//#define ROBOT_FRAME "base_link"
#define SWITCH_FRAME "calibration_grid_map"

using namespace std;

struct grid_map_coordinate {
	int x, y;
	string name;
	int unobs_state;
};

string robot_frame;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

bool goalReached;
double offset_positionX, offset_positionY, max_observation_distance;

double degToRad(double degree) {
	return degree * M_PI / 180.0;
}

bool rotateMobileBase (MoveBaseClient &ac, double goalX, double goalY, double goalYaw)
{
	move_base_msgs::MoveBaseGoal goal;

		//we'll send a goal to the robot to move 1 meter forward
		goal.target_pose.header.frame_id = "Pioneer3AT/map";
		goal.target_pose.header.stamp = ros::Time::now();
		
		goal.target_pose.pose.position.x = goalX;
		goal.target_pose.pose.position.y = goalY;

		goal.target_pose.pose.orientation.z = sin(goalYaw / 2.0);
		goal.target_pose.pose.orientation.w = cos(goalYaw / 2.0);

		ROS_INFO("Sending goal");
		sleep(2);

		ac.sendGoal(goal);

		ac.waitForResult(ros::Duration(0, 0));

		if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
			ROS_INFO("Action terminated");
		else {
			ROS_ERROR("Action failed! (rotate base to yaw = %d )", goalYaw);
			return false;
		}

		return true;
}

bool navigateBaseToPose(MoveBaseClient &ac, double goalX, double goalY,
		double orientZ) {
	move_base_msgs::MoveBaseGoal goal;

	//we'll send a goal to the robot to move 1 meter forward
	goal.target_pose.header.frame_id = "Pioneer3AT/map";
	goal.target_pose.header.stamp = ros::Time::now();

	goal.target_pose.pose.position.x = goalX;
	goal.target_pose.pose.position.y = goalY;

	goal.target_pose.pose.orientation.z = sin(orientZ / 2.0);
	goal.target_pose.pose.orientation.w = cos(orientZ / 2.0);

	ROS_INFO("Sending goal");
	sleep(2);

	ac.sendGoal(goal);

	ac.waitForResult(ros::Duration(0, 0));

	if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("Action terminated");
	else {
		ROS_ERROR("Action failed! (navigate base to x = %d, y = %d )", goalX,
				goalY);
		return false;
	}

	return true;
}

void getGoalCoordinate(double &goalX, double &goalY, double &goalYaw,
		grid_map_coordinate observable_state, double robot_displacement_step,
		int A) {

	goalX = robot_displacement_step * (observable_state.x - 1)
			+ offset_positionX;
	goalY = robot_displacement_step * (observable_state.y - 1)
			+ offset_positionY;

	cout << goalX << " " << goalY << " D = " << robot_displacement_step
			<< " oX = " << offset_positionX << " oY = " << offset_positionY;

	switch (A) {
	case 0:
		goalYaw = degToRad(-180.0);
		break;
	case 1:
		goalYaw = degToRad(+90.0);
		break;
	case 2:
		goalYaw = degToRad(0.0);
		break;
	case 3:
		goalYaw = degToRad(-90.0);
		break;
	}
	cout << " yaw = " << goalYaw << endl;

}

int getGridCoordinateDistance(grid_map_coordinate r, grid_map_coordinate s) {
	return abs(r.x - s.x) + abs(r.y - s.y);
}

void performActionOnGridMap(grid_map_coordinate &state, int a) {
	switch (a) {
	case 0:
		state.x = state.x - 1;
		break;
	case 1:
		state.y = state.y + 1;
		break;
	case 2:
		state.x = state.x + 1;
		break;
	case 3:
		state.y = state.y - 1;
		break;
	}
}

bool initSwitches_pose(std::vector<tf::Pose> &p, ros::NodeHandle n) {
	return true;
}

tf::Pose getPose_xyz_Euler(double x, double y, double z, double roll,
		double pitch, double yaw) {
	tf::Pose p;
	tf::Vector3 O;
	tf::Quaternion Q;
	O.setX(x);
	O.setY(y);
	O.setZ(z);
//		O.setW(0);
	Q.setEuler(yaw, pitch, roll);
	p.setOrigin(O);
	p.setRotation(Q);
	return p;
}

double distance_robot_switch(tf::Pose robot) {
	double distance = 0.0;

	distance = sqrt(
			pow(robot.getOrigin().getX(), 2)
					+ pow(robot.getOrigin().getY(), 2));

	return distance;
}

tf::Pose getRobotPose(tf::TransformListener &l, string switch_name) {
	tf::Pose p;
	tf::StampedTransform frame_;
	if (l.waitForTransform(robot_frame, switch_name, ros::Time(0),
			ros::Duration(5.0))) //ros::Time::now()
					{
		l.lookupTransform(robot_frame, switch_name, ros::Time(0), frame_);
		p.setOrigin(frame_.getOrigin());
		p.setRotation(frame_.getRotation());
	} else {
		ROS_ERROR("TF robot and switch frame are not found!");
		p.setIdentity();
		cout << robot_frame << " - " << switch_name << endl;
	}
	return p;
}

//void baseMotionResult(const move_base_msgs::MoveBaseActionResult::ConstPtr& msg) {
//
//	//std::cout << "msg received: " << msg.get() << std::endl;
//	goalReached = true;
//	ROS_INFO("Mobile base reached goal.");
//}

int observationMapper(vector<grid_map_coordinate> switches, int nrSwithes) {
	int base = 1;
	int ID = 0;
	for (int i = 0; i < nrSwithes; i++) {
		ID = ID + base * switches[nrSwithes - i - 1].unobs_state;
		base = base * 3;
	}

	return ID;
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "centralized_Controller");

	ros::NodeHandle n;
	double robot_displacement_step;
	int nrSwitches;
	vector<grid_map_coordinate> switches;
	std::string move_base;
	grid_map_coordinate robot_grid_coordinate;

	n.param<double>("/cenral_controller/displacemant_action_lenght",
			robot_displacement_step, 0.65);
	n.param<double>("/cenral_controller/max_observation_tolarence",
			max_observation_distance, 1.2);
	n.param<double>("/cenral_controller/zero_positionX", offset_positionX, 0.0);
	n.param<double>("/cenral_controller/zero_positionY", offset_positionY, 0.0);

	n.param<string>("/central_controller/robot_frame_name", robot_frame,
			"base_link");
	n.param<int>("/cenral_controller/robot_gridcoordX", robot_grid_coordinate.x,
			1);
	n.param<int>("/cenral_controller/robot_gridcoordY", robot_grid_coordinate.y,
			1);

	n.param<int>("/cenral_controller/nuber_of_switches", nrSwitches, 1);
	ostringstream oss;
	string param_name, sw_name;
	int x_, y_;
	for (int i = 0; i < nrSwitches; i++) {
		oss << "/cenral_controller/sw_frame_name_" << i;
		param_name = oss.str();
		n.param<string>(param_name, sw_name, "calibration_grid_map");
		cout << i << endl;
		cout << param_name << " = " << sw_name << endl;

		oss << "_x";
		param_name = oss.str();
		n.param<int>(param_name, x_, 0);

		cout << param_name << " = " << x_ << endl;

		oss.str("");
		oss.clear();
		oss << "/cenral_controller/sw_frame_name_" << i << "_y";
		param_name = oss.str();
		n.param<int>(param_name, y_, 0);

		cout << param_name << " = " << y_ << endl;

		grid_map_coordinate item;
		item.name = sw_name;
		item.x = x_;
		item.y = y_;
		item.unobs_state = 2;

		switches.push_back(item);
		oss.str("");
		oss.clear();
	}

	double x, y, z, w;

	n.param<std::string>("/Pioneer3AT_navigoal/move_base", move_base,
			"/Pioneer3AT/move_base");
	n.param<double>("/Pioneer3AT_navigoal/x", x, 0.0);
	n.param<double>("/Pioneer3AT_navigoal/y", y, 0.0);
	n.param<double>("/Pioneer3AT_navigoal/z", z, 0.00);
	n.param<double>("/Pioneer3AT_navigoal/w", w, 1.00);

	//NAVIGATION ACTION
	//action client for navigation
	//tell the action client that we want to spin a thread by default
	MoveBaseClient ac(move_base, true);
	//wait for the action server to come up
	while (!ac.waitForServer(ros::Duration(5.0))) {
		ROS_INFO("Waiting for the move_base action server to come up");
	}

	// OBSERVATION - switch state
	// camera should be oriented already to the SW
	ros::ServiceClient client_SensingSW = n.serviceClient<
			switch_state_detector::GetState>("detect_switch_state_enhenced");

	// arm rotates around to find chess-board, if found orient arm toward board
	ros::ServiceClient client_CBsearch = n.serviceClient<
			arm_action_server::FindChessBoard>("find_chess_board");

	// ACTION F
	// arm in range of CB, has visual contact with CB to perform flip action
	ros::ServiceClient client_FlippSW = n.serviceClient<
			arm_action_server::FlippSwitch>("flipp_switch");

	// PLANNER
	// request a new action based on current observation
	ros::ServiceClient client_Planner = n.serviceClient<
			pomdp_planner::PlannerAction>("getNextAction_planner");

	// ROBOT position listener
	tf::TransformListener listenre_robotPose;

	// messages from service servers
	switch_state_detector::GetState switchState_msg;
	arm_action_server::FindChessBoard chessBoard_msg;
	arm_action_server::FlippSwitch switchFlip_msg;
	pomdp_planner::PlannerAction plannerAction_msg;

	int actionID = -1, observationID = 8;

//	std::vector<tf::Pose> sw;
// 	std::vector<int> sw_state;

	int sw_state;
	tf::Pose sw = getPose_xyz_Euler(0, 0, 0, 0, 0, 0);
	tf::Pose robot_tf;
	double yaw, roll, pitch; // relative orientation of robot to chess board
	double goalX, goalY, goalYaw;

	cout << "Init done\n";
	while (ros::ok()) {
		// get next action
		plannerAction_msg.request.observation = observationID;

		if (client_Planner.call(plannerAction_msg)) {
			actionID = plannerAction_msg.response.action;

			cout << "A = " << actionID << endl;
//			robot = getRobotPose(listenre_robotPose);
//			robot.getBasis().getEulerYPR(yaw, pitch, roll);

//			if (actionID > 3) {
			if (actionID == 4) {
				for (int i = 0; i < nrSwitches; i++) {

					//check from coordinate distance >1 -> unobservable
					if (getGridCoordinateDistance(robot_grid_coordinate,
							switches[i]) == 0) {
						robot_tf = getRobotPose(listenre_robotPose,
								switches[i].name);
						robot_tf.getBasis().getEulerYPR(yaw, pitch, roll);
						cout << "O: yaw = " << yaw << endl;
					}
				}

				rotateMobileBase(ac,goalX, goalY, goalYaw + yaw);

				chessBoard_msg.request.distance = 0.0; //input not used
				chessBoard_msg.request.yaw = 0.0; //yaw; // if robot position is known than give as an input
				// search for the CB before flip can performed
				if (client_CBsearch.call(chessBoard_msg)) {
					cout << "A - F: cb found distance "
							<< chessBoard_msg.response.distance << endl;
					switchFlip_msg.request.distance = 0.03; // distance between arm eef and panel
					switchFlip_msg.request.yaw = 0.0; // input not used
					if (client_FlippSW.call(switchFlip_msg)) {
						std::cout << "A - F: SUCCESFULL.\n";
					} else {
						ROS_ERROR(
								"Flip service not responding! (arm_action_server)");
					}
				} else {
					ROS_ERROR(
							"Chess-board not found while performing flip action!");
				}
			}
			//else
//				{
//					sleep(3.0);
//				}
			//} else {

			if (actionID < 4) {
				performActionOnGridMap(robot_grid_coordinate, actionID);
				getGoalCoordinate(goalX, goalY, goalYaw, robot_grid_coordinate,
						robot_displacement_step, actionID);

				//TODO test navigation function
				navigateBaseToPose(ac, goalX, goalY, goalYaw);
				cout << "A - NESW: at coordinate: " << robot_grid_coordinate.x
						<< " " << robot_grid_coordinate.y << endl;
				cout << "A - NESW: goal (x, y) = (" << goalX << ", " << goalY
						<< ") yaw = " << goalYaw << endl;
			}

			if (actionID == 5) {
				cout << "A - X: stay at current location \n";
				sleep(5.0);
			}

			// check all switches
			if (actionID == 6) {
				for (int i = 0; i < nrSwitches; i++) {

					//check from coordinate distance >1 -> unobservable
					if (getGridCoordinateDistance(robot_grid_coordinate,
							switches[i]) < 2) {
						robot_tf = getRobotPose(listenre_robotPose,
								switches[i].name);
						//robot_tf.getBasis().getEulerYPR(yaw, pitch, roll);
						yaw = atan2(robot_tf.getOrigin().y(), robot_tf.getOrigin().x());

//						yaw = yaw;

						cout << "! O: yaw = " << yaw  << " x = " << robot_tf.getOrigin().x() << " y = " <<  robot_tf.getOrigin().y() << endl;

						//check from static TF distance > max_o_d -> unobs
						if (distance_robot_switch(robot_tf)
								< max_observation_distance) {
							cout << "CB in TF range SW" << i << endl;
							// get the states of SW #i
							chessBoard_msg.request.distance = 0.0; //input not used
							chessBoard_msg.request.yaw = yaw; // if robot position is known than give as an input
							if (client_CBsearch.call(chessBoard_msg)) {
								cout << "O: cb found distance "
										<< chessBoard_msg.response.distance
										<< endl;

								if (chessBoard_msg.response.distance
										< max_observation_distance) {
									cout << "O: cb in range \n";
									switchState_msg.request.distance =
											chessBoard_msg.response.yaw;

									if (client_SensingSW.call(
											switchState_msg)) {
										switches[i].unobs_state = 1
												- switchState_msg.response.ON;
										cout << "O: observed state = "
												<< switches[i].unobs_state
												<< endl;
									} else {
										ROS_ERROR(
												"Observer service not responding! (switch state sensing)");
									}
								} else // switch is out of range => state is unobservable
								{
									cout << "O cb out of range\n";
									switches[i].unobs_state = 2;
								}
							} else // CB not found, no measurement can be done
							{
								ROS_ERROR(
										"Chess-board not found while checking switch %d state!",
										i);
								switches[i].unobs_state = 2;
							}
						} else {
							cout << "O cb out of range regarding TF "
									<< distance_robot_switch(robot_tf)
									<< " for SW" << i << "\n";
							switches[i].unobs_state = 2;
						}

					} else {
						cout << "O cb out of range at index "
								<< robot_grid_coordinate.x << " "
								<< robot_grid_coordinate.y << " for SW" << i
								<< " at: " << switches[i].x << " "
								<< switches[i].y << " dist:"
								<< getGridCoordinateDistance(
										robot_grid_coordinate, switches[i])
								<< "\n";
						switches[i].unobs_state = 2;
					}

				}
				observationID = observationMapper(switches, nrSwitches);
			} else {
				observationID = 8;
			}
			//TODO do the mapping - O

			cout << "LOOP END: new observation " << observationID << endl;
		} else {
			ROS_ERROR("POMDP planner service not responding!");
			return -1;
		}

	} // end of while()

	return 0;
}
