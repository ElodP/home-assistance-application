#include "ros/ros.h"
#include "switch_state_detector/GetState.h"
#include "arm_action_server/FindChessBoard.h"
#include "arm_action_server/FlippSwitch.h"
#include <stdbool.h>
#include <cstdlib>
#include "std_msgs/String.h"
#include "move_base_msgs/MoveBaseActionResult.h"

// for file handling:
#include <iostream>
#include <fstream>

// for ft of CB
//TF listener
#include <tf/transform_listener.h>
#include <tf/tf.h>

using namespace std;

bool goalReached;

void baseMotionResult(
		const move_base_msgs::MoveBaseActionResult::ConstPtr& msg) {

	//std::cout << "msg received: " << msg.get() << std::endl;
	goalReached = true;
	ROS_INFO("Mobile base reached goal.");
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "arm_Manipulation_Sensing_Controller");

	ros::NodeHandle n;
	ros::ServiceClient client_SensingSW = n.serviceClient<
			switch_state_detector::GetState>("detect_switch_state_enhenced"); //
	ros::ServiceClient client_CBsearch = n.serviceClient<
			arm_action_server::FindChessBoard>("find_chess_board");
	ros::ServiceClient client_FlippSW = n.serviceClient<
			arm_action_server::FlippSwitch>("flipp_switch");

	switch_state_detector::GetState switchState_msg;
	arm_action_server::FindChessBoard chessBoard_msg;
	arm_action_server::FlippSwitch switchFlip_msg;
	switchState_msg.request.distance = 3.5;

	bool SWstateON = false;
	std::cout << "==== Action flip is SW ON ====\n\n";
	// Find Chess board and orientate toward CB.
	goalReached = false;

	switchState_msg.request.distance = 0;
	ofstream myfile;
	//myfile.open("test_pdf.txt", std::ofstream::out | std::ofstream::app);

	tf::StampedTransform WRtransfrom_inCBframe, CBinArmFrame;
	tf::TransformListener chessBoard_listener;
	char stop = 'c';
	printf("To stop data collection enter x \n");
	int i = 0;
	double r,p,y = 0 ;
	//for (int i = 0; i < 10; i++)
	while (stop != 'x'){
		myfile.open("test_pdf.txt", std::ofstream::out | std::ofstream::app);
		printf(" %d: ", i );
		
		switchState_msg.request.distance = y;

		if (client_SensingSW.call(switchState_msg)) {

			myfile << i << ", "; // << switchState_msg.response.stateON << " ";
			if (switchState_msg.response.ON)
				myfile<< "1 ";
			else
				myfile<< "0 ";
			try {

				chessBoard_listener.lookupTransform("camera_front",
						"calibration_grid", ros::Time(0), CBinArmFrame);
				sleep(3.0);
				CBinArmFrame.getBasis().getRPY(r,p,y);
				//printf("r %f, p %f, y %f \n", r, p, y);
				myfile << CBinArmFrame.getOrigin().getZ() << ", " << r*180.0/M_PI;

				printf("distance %f, angle %f ", CBinArmFrame.getOrigin().getZ(), 180- abs(r*180.0/M_PI));
			} catch (tf::TransformException ex) {
				ROS_ERROR("CB not found - %s", ex.what());
				myfile<< ", -1, -1";
			}

			//printf("Switch state ON ");
			printf(switchState_msg.response.ON ? "true " : "false ");
			printf("size %f \n", switchState_msg.response.stateON);
			myfile << endl;

		} else {
			ROS_ERROR("\n\n  Switch state detector service is offline! \n");
			//return -1;
		}
		i++;
		myfile.close();
		std::cin >> stop;

	}
	// Check SW state

	// If SW = ON => flip, back to initial arm pose.

	// else wait.
	printf("test termianted\n");

	return 0;
}
