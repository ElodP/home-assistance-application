#include "ros/ros.h"
#include "switch_state_detector/GetState.h"
#include "arm_action_server/FindChessBoard.h"
#include "arm_action_server/FlippSwitch.h"
#include <stdbool.h>
#include <cstdlib>
#include "std_msgs/String.h"
#include "move_base_msgs/MoveBaseActionResult.h"

bool goalReached;

void baseMotionResult(const move_base_msgs::MoveBaseActionResult::ConstPtr& msg) {

	//std::cout << "msg received: " << msg.get() << std::endl;
	goalReached = true;
	ROS_INFO("Mobile base reached goal.");
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "arm_Manipulation_Sensing_Controller");

	ros::NodeHandle n;
	ros::ServiceClient client_SensingSW = n.serviceClient<
			switch_state_detector::GetState>("detect_switch_state");
	ros::ServiceClient client_CBsearch = n.serviceClient<
			arm_action_server::FindChessBoard>("find_chess_board");
	ros::ServiceClient client_FlippSW = n.serviceClient<
			arm_action_server::FlippSwitch>("flipp_switch");

	switch_state_detector::GetState switchState_msg;
	arm_action_server::FindChessBoard chessBoard_msg;
	arm_action_server::FlippSwitch switchFlip_msg;
	switchState_msg.request.distance = 3.5;

	bool SWstateON = false;
	std::cout << "==== Action flip is SW ON ====\n\n";
	// Find Chess board and orientate toward CB.
	goalReached = false;

	ros::Subscriber sub = n.subscribe("/Pioneer3AT/move_base/result", 1, baseMotionResult);
	//ros::spin();
	while (!goalReached)
	{
		//wait till base reaches goal
		sleep(1);
		ros::spinOnce();
	}
	std::cout << std::endl;

	 std::cout << "==== Goal Reached!  ====\n\n";

	chessBoard_msg.request.distance = 0.0;
	chessBoard_msg.request.yaw = 0.0;
	std::cout << "---call find CD action \n";
	if (client_CBsearch.call(chessBoard_msg)) {
		std::cout << "Chess board found at " << chessBoard_msg.response.distance
				<< std::endl;

//		switchState_msg.request.distance = chessBoard_msg.response.distance;
//		if (client_SensingSW.call(switchState_msg))
//		{

		//printf("Switch state ON ");
//		printf(switchState_msg.response.ON ? "true " : "false ");
//		printf("size %f \n", switchState_msg.response.stateON);

		//if (switchState_msg.response.ON) {
		//	std::cout << "---call flip action \n";
			switchFlip_msg.request.distance = 0.03;
			switchFlip_msg.request.yaw = 0.0;
			if (client_FlippSW.call(switchFlip_msg)) {

				std::cout << "The switch is turned off.\n";

			} else {
				ROS_ERROR(
						"\n\n  Flipping switch failed by arm_action_server! \n");
				return -1;
			}
//		} else {
//			printf("The switch was OFF, no flip action was taken!\n");

//		}
//		} else {
//			ROS_ERROR("\n\n  Sensing server failed detection! \n");
//			return -1;
//		}
	} else {
		ROS_ERROR("\n\n  Chess-board was not found by arm_action_server! \n");
		return -1;
	}

	// Check SW state

	// If SW = ON => flip, back to initial arm pose.

	// else wait.
	printf("-Flip action manager terminated.\n");

	return 0;
}
