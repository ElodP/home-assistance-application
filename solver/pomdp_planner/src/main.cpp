#include "ros/ros.h"
#include "solverOP_lib.hpp"
#include "pomdp_planner/PlannerAction.h"
// for file handling:
#include <iostream>
#include <fstream>

using namespace std;

POMDPplanner *myplanner = 0;
int current_action;

bool srvCallback(pomdp_planner::PlannerAction::Request &req, pomdp_planner::PlannerAction::Response &res)
{
	int current_observation = req.observation;

	// position in nextIteration is not necessary due to changes in planner 25.11.15
	current_action = myplanner->nextIteration(current_observation, 0);
	res.action = current_action;

	if (current_action < 0 || current_action > 6)
	{
		ROS_ERROR("The returned action is not in the acceptable ID range");
		return false;
	}

	return true;
}


int main(int argc, char **argv) {
	ros::init(argc, argv, "POMDP_planer");

	ros::NodeHandle n;


	string model, input, output;
	int budget, sLength, sTime;
	bool sim, vis;

	n.getParam("/pomdp_planner/model", model);
	n.getParam("/pomdp_planner/UnObsInitStateFile", input);
	n.getParam("/pomdp_planner/outputFile", output);

	n.getParam("/pomdp_planner/budget", budget);
	n.getParam("/pomdp_planner/simLength", sLength);
	n.getParam("/pomdp_planner/simTime", sTime);

	n.getParam("/pomdp_planner/simulate", sim);
	n.getParam("/pomdp_planner/visualise", vis);

	cout << "model: " << model.c_str()  << " b " << budget  << endl;

//	myplanner = POMDPplanner("./model/model1x3.pomdpx",250,true,false,25,2,"out.txt","./model/init1x3.txt");
	myplanner = new POMDPplanner(model.c_str(), budget, sim, vis, sLength, sTime, output.c_str(), input.c_str());

	ros::ServiceServer getNextAction = n.advertiseService("getNextAction_planner", srvCallback);
	ros::spin();

	cout << " \nPlanner service server is ON\n\n";
//	int i = myplanner.nextIteration(2, 0);



	return 0;
}
