# README #

This repository is a ROS Indigo package collection implementing our **domestic robot ** application, published under BSD license. 

**Ref to:** Elod P�ll, Levente Tam�s, Lucian Busoniu, Analysis and a Home Assistance Application of Online AEMS2 Planning. Accepted at IEEE/RSJ International Conference on Intelligence Robots and Systems (IROS-16), 2016


## Important package dependencies: ##

* arm control: MoveIt!, dynamixel;
* image porcessing: marker_pose_detection, aruco, uvc_camera, pal_vision_segmentation;
* mobile robot: rosaria 
* OpenCV 2.4.11

## Repository content description ##
This is a demo application of a domestic robot application, where a mobile robot keeps electrical switches turned off in a known domestic environment. The high level controller is the AEMS2 online POMDP solver. This work is published at IROS16 in this [paper](Link URL) and other details can be found [here](https://sites.google.com/site/timecontroll/home/pomdp-solver---assistive-robotics).

## How to use it##

The implemented POMDP solver can be used stand alone, see repository [here](https://bitbucket.org/ElodP/extended-appl-with-aems2/overview). It is not necessary  to download this repository, because the **pomdp_planner** node should automatically download and compile from the above mentioned repository. 

### Real system setup ###
Used hardware: pioneer 3AT, Sick LMS200, Cyton Gamma 1500, Logitech C615 camera (the calibration file is: vision/ost1280x720ML.txt).
 
Experimental environment: electrical switch with LED display, marker (./vision/OpenCV_Chessboards2.pdf)


ROS commands to run the demo application:

```
#!bash
$ roslaunch arm_manipulation_sensing low_level_nodes.launch
$ roslaunch arm_manipulation_sensing desktop_nodes.launch
$ roslaunch arm_manipulation_sensing centralized_controller.launch
```

## Contact ##
Elod Pall
sites.google.com/view/timecontrol/